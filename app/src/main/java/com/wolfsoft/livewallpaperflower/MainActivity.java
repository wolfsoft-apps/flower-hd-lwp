package com.wolfsoft.livewallpaperflower;

import android.app.WallpaperManager;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.adincube.sdk.AdinCube;
import com.crashlytics.android.Crashlytics;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.fabric.sdk.android.Fabric;
import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;

public class MainActivity extends AppCompatActivity {

    private static final String ADS_API_KEY = BuildConfig.ADS_API_KEY;

    @BindView(R.id.wallpaper)
    GifImageView wallpaperView;

    protected void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        initAds();

        try {
            GifDrawable gifFromResource = new GifDrawable(getResources(), R.raw.wallpaper);
            wallpaperView.setImageDrawable(gifFromResource);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initAds() {
        AdinCube.setAppKey(ADS_API_KEY);
        AdinCube.Interstitial.init(this);
    }

    @OnClick(R.id.set_wallpaper)
    public void onSetWallpaperButtonPressed() {
        try {
            Intent i = new Intent();
            i.setAction(WallpaperManager.ACTION_CHANGE_LIVE_WALLPAPER);
            String pkg = CustomWallpaperService.class.getPackage().getName();
            String cls = CustomWallpaperService.class.getCanonicalName();
            i.putExtra(WallpaperManager.EXTRA_LIVE_WALLPAPER_COMPONENT, new ComponentName(pkg, cls));
            startActivity(i);
        } catch (Exception e) {
            try {
                startActivity(new Intent(WallpaperManager.ACTION_LIVE_WALLPAPER_CHOOSER));
            } catch (Exception ex) {
                finish();
            }
        }

        if (AdinCube.Interstitial.isReady(this)) {
            AdinCube.Interstitial.show(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (AdinCube.Interstitial.isReady(MainActivity.this)) {
                    AdinCube.Interstitial.show(MainActivity.this);
                }
            }
        }, 500);
    }
}