package com.wolfsoft.livewallpaperflower;

import android.graphics.Canvas;
import android.graphics.Movie;
import android.os.Handler;
import android.os.SystemClock;
import android.service.wallpaper.WallpaperService;
import android.util.Log;
import android.view.SurfaceHolder;

import java.io.IOException;
import java.io.InputStream;

public class CustomWallpaperService extends WallpaperService {
    private static final String TAG = CustomWallpaperService.class.getCanonicalName();

    private static final Handler wallpaperHandler = new Handler();
    private static final int wallpaperId = R.raw.wallpaper;

    @Override
    public Engine onCreateEngine() {
        try {
            return new CustomEngine();
        } catch (IOException e) {
            Log.w(TAG, "Error creating CustomEngine", e);
            stopSelf();
            return null;
        }
    }

    private class CustomEngine extends Engine {
        private final Movie wallpaperMovie;
        private final int movieDuration;
        private final Runnable wallpaperRunnable;
        float mScaleX;
        float mScaleY;
        int mWhen;
        long mStart;

        CustomEngine() throws IOException {
            InputStream is = getResources().openRawResource(wallpaperId);
            if (is != null) {
                try {
                    wallpaperMovie = Movie.decodeStream(is);
                    movieDuration = wallpaperMovie.duration();
                } finally {
                    is.close();
                }
            } else {
                throw new IOException("Unable to open R.raw.wallpaper");
            }

            mWhen = -1;
            wallpaperRunnable = new Runnable() {
                public void run() {
                    drawFrame();
                }
            };
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
            wallpaperHandler.removeCallbacks(wallpaperRunnable);
        }

        @Override
        public void onVisibilityChanged(boolean visible) {
            super.onVisibilityChanged(visible);
            if (visible) {
                drawFrame();
            } else {
                wallpaperHandler.removeCallbacks(wallpaperRunnable);
            }
        }

        @Override
        public void onSurfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            super.onSurfaceChanged(holder, format, width, height);
            mScaleX = width / (1f * wallpaperMovie.width());
            mScaleY = height / (1f * wallpaperMovie.height());
            drawFrame();
        }

        @Override
        public void onOffsetsChanged(float xOffset, float yOffset, float xOffsetStep,
                                     float yOffsetStep, int xPixelOffset, int yPixelOffset) {
            super.onOffsetsChanged(xOffset, yOffset, xOffsetStep, yOffsetStep, xPixelOffset, yPixelOffset);
            drawFrame();
        }

        void drawFrame() {
            tick();
            SurfaceHolder surfaceHolder = getSurfaceHolder();
            Canvas canvas = null;
            try {
                canvas = surfaceHolder.lockCanvas();
                if (canvas != null) {
                    drawFrame(canvas);
                }
            } finally {
                if (canvas != null) {
                    surfaceHolder.unlockCanvasAndPost(canvas);
                }
            }
            wallpaperHandler.removeCallbacks(wallpaperRunnable);
            if (isVisible()) {
                wallpaperHandler.postDelayed(wallpaperRunnable, 1000L / 25L);
            }
        }

        void tick() {
            if (mWhen == -1L) {
                mWhen = 0;
                mStart = SystemClock.uptimeMillis();
            } else {
                long mDiff = SystemClock.uptimeMillis() - mStart;
                mWhen = (int) (mDiff % movieDuration);
            }
        }

        void drawFrame(Canvas canvas) {
            canvas.save();
            canvas.scale(mScaleX, mScaleY);
            wallpaperMovie.setTime(mWhen);
            wallpaperMovie.draw(canvas, 0, 0);
            canvas.restore();
        }
    }
}